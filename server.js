const express = require('express'),
    app = express(),
    port = process.env.PORT || 3012,
    url = 'https://api.sanakan.pl/api/',
    axios = require('axios'),
    cors = require('cors');
require('dotenv').config();

const jwtCode = process.env.TOKEN || 'nope';
console.log(process.env.NODE_ENV);
console.log(jwtCode);

app.use(cors());
var endTokenData = new Date();
var token = '';
var getToken = function () {
    if(endTokenData <= new Date()) {
        let options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            data: JSON.stringify(jwtCode),
            url: url + 'Token',
        };
        let responseData = '';
        axios(options)
            .then(response => {
                responseData = response.data;
                console.log('r-token', responseData);
                token = responseData.token;
                endTokenData = new Date(responseData.expire);
            }).catch(error => {
            console.log(error);
        }).finally(() => {

            console.log('saved-token', token);
            console.log(endTokenData);
        });
    }
};

app.route('/user/:userId/list').get(async (request, response) => {
    console.log(request.params);
    getToken();
    const options = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + token,
        },
        url: url + 'Waifu/user/' + request.params.userId + '/cards',
    };
    return response.json(await axios(options)
        .then(response => {
            const responseData = response.data;
            // console.log(responseData);
            return responseData;
        }).catch(error => {
            console.log(error);
        }));
});

app.route('/user/:userId/wishlist').get(async (request, response) => {
    console.log(request.params);
    getToken();
    const options = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + token,
        },
        url: url + 'Waifu/user/shinden/' + request.params.userId + '/wishlist',
    };
    return response.json(await axios(options)
        .then(response => {
            let responseData = response.data;
            // console.log(responseData);
            return responseData;
        }).catch(error => {
            console.log(error);
        }));
});

app.route('/user/find/:userString').get(async (request, response) => {
    getToken();
    console.log('token ahead');
    console.log('token: ', token);
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        },
        data: JSON.stringify(request.params.userString),
        url: url + 'User/find',
    }

    return response.json(await axios(options)
        .then(response => {
            return response.data;
        }).catch(error => {
            console.log(error);
        }));
})

app.listen(port);

console.log('RESTful API server started on: ' + port);